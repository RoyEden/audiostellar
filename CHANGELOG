# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
- Units instead of Modes

## [0.10.0] - 2020-05-05
### Added
- UMAP and PCA visualization
- New features: Spectral Centroid, RMS, Chromagram
- ofxPDSP as sound engine
- Audio settings window
- Cluster names
- Output channels for sounds and clusters
- Mute sounds and clusters
- OSC support in Particle mode
- OSC documentation
- Drag to emit particles in Particle mode
- Initial TidalCycles support (proof of concept)
- Visual Studio project

### Changed
- Data analysis settings window

### Fixed
- Several bug fixes


## [0.9.0] - 2019-08-16
### Added
- Mini tutorial added
- Machine Learning process is now full python (via pyinstaller)
- data-analysis repo is now part of AudioStellar main repo
- GUI has been revamped, we only have one window now
- We now support OSC. First basic approach is implemented.
- More user friendly defaults for Modes' parameters
- Better handling of invalid session files
- stellar.json for general settings


## [0.8.0] - 2019-06-02
### Added
- First public release
