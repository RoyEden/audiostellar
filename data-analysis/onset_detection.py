import librosa
import numpy as np
import scipy.io.wavfile
from pydub import AudioSegment, effects  
import os
import argparse

import sys

parser = argparse.ArgumentParser()
parser.add_argument("audioFilePath", help = "Path to audio file")
parser.add_argument("--folder", help = "Name of the folder in which the slices will be recorded. Default: name of audio file")

parser.add_argument("--window_max", type = float, default = 0.03)
parser.add_argument("--window_avg", type = float, default = 0.1)
parser.add_argument("--delta", type = float, default = 0.07)
parser.add_argument("--backtrack", default = False, action = "store_true")

parser.add_argument("--save", default = False, action = "store_true")
parser.add_argument("--fade", type = int, default = 1000)
parser.add_argument("--normalize", default = False, action = "store_true")

args = parser.parse_args()

completePath = os.path.realpath(args.audioFilePath)
audioFilePathDirectoryName, audioFile = os.path.split(completePath)
fileName, ext = os.path.splitext(audioFile)

def findOnsetsAndCut( window_max, window_avg, delta, backtrack ):
  global onset_frames, onset_times, times
  
  hop_length= 512
  o_env = librosa.onset.onset_strength(segmento, sr=sr)
  times = librosa.times_like(o_env, sr=sr)

  window_max_final = window_max * sr//hop_length
  window_avg_final = window_avg * sr//hop_length

  onset_frames = librosa.onset.onset_detect(onset_envelope=o_env, sr=sr, hop_length= hop_length,
                                            backtrack=True,
                                            delta=delta, 
                                            # wait=0, 
                                            pre_avg= window_avg_final, post_avg= window_avg_final + 1, 
                                            pre_max = window_max_final, post_max = window_max_final + 1)

  onset_times = librosa.frames_to_time(onset_frames, sr)


def loadFile(filename):
  global x, sr, segmento

  target = args.audioFilePath

  if not os.path.isfile(target):
    return
  else:
    x, sr = librosa.load(target, sr=44100)

  segmento = x
def fadeNormalizeSave(folder, fadeSamples = 1000, normalize = True):
  
  segmentoFade = np.copy(segmento)


  for f in range(1,len(onset_frames)):
      sampleFinish = librosa.core.frames_to_samples( onset_frames[f] )

      j = fadeSamples
      for i in np.arange(sampleFinish-fadeSamples, sampleFinish+1):
          segmentoFade[i] *= j/fadeSamples
          j-=1

  if args.folder is None:
      args.folder = fileName

  carpetaFinal = f"{audioFilePathDirectoryName}/{args.folder}/"
  os.mkdir(carpetaFinal)
  

  for i in range(len(onset_frames)-1):
      timeF = librosa.core.frames_to_samples( onset_frames[i] )
      timeT = librosa.core.frames_to_samples( onset_frames[i+1] )

      archivoAGuardar = f"{carpetaFinal}/{i}.wav"

      scipy.io.wavfile.write( archivoAGuardar ,sr, segmentoFade[timeF:timeT])
      
      if normalize:
          rawsound = AudioSegment.from_wav(archivoAGuardar)  
          normalizedsound = effects.normalize(rawsound)  
          normalizedsound.export(archivoAGuardar, format="wav")


loadFile(args.audioFilePath)

findOnsetsAndCut(args.window_max, args.window_avg, args.delta, args.backtrack)

if args.save:
    fadeNormalizeSave(args.folder, args.fade, args.normalize)

print(librosa.core.frames_to_samples( onset_frames ))
