#pragma once

#include "ofMain.h"

namespace ColorPaletteGenerator {
    static vector<int> colors = {0x1f78b4,
                                 0xff7f00,
                                 0xfdbf6f,
                                 0x9e1213,
                                 0xfb9a99,
                                 0x4325af,
                                 0xa6cee3,
                                 0x0991af,
                                 0xcab2d6,
                                 0xa58ac2,
                                 0xffff99,
                                };

    static int originalColorCount = colors.size();
    static ofColor stripeMainColor =  ofColor(0x2e2e2e);
    static const ofColor noiseColor =  ofColor(0xc7c7c7);

    static ofColor getColor(int idx){
        if ( idx < 0 ) {
            return noiseColor;
        }
        return ofColor::fromHex( colors[ idx % originalColorCount ] );
    }

    static ofColor getRandomOriginalColor() {
        return ofColor::fromHex( colors[ ceil(ofRandom(0, originalColorCount)) ] );
    }
};
