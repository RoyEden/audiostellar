#include "UnitBehaviorDragToPlay.h"

void UnitBehaviorDragToPlay::mouseDragged(ofVec2f p, int button) {
    if(button == 0){
        Sound * hoveredSound = Sounds::getInstance()->getNearestSound(p);
        if ( hoveredSound != nullptr ) {
            if ( hoveredSound != lastPlayedSound ) {
                parent->playSound(hoveredSound);
                lastPlayedSound = hoveredSound;
            }
        } else {
            lastPlayedSound = nullptr;
        }
    } else {
        lastPlayedSound = nullptr;
    }
}
