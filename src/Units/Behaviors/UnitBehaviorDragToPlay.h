#pragma once

#include "ofMain.h"
#include "../../Sound/Sounds.h"
#include "UnitBehavior.h"

class UnitBehaviorDragToPlay: public UnitBehavior{
private:
    Sound * lastPlayedSound = nullptr;

public:
    UnitBehaviorDragToPlay(Unit * parentUnit) : UnitBehavior(parentUnit) {};
    const string getName() { return "UnitBehaviorDragToPlay"; }

    void mouseDragged(ofVec2f p, int button);
};
