#include "UnitBehaviorOSC.h"

UnitBehaviorOSC::UnitBehaviorOSC(Unit * parentUnit)
    : UnitBehavior(parentUnit)
{
    ofAddListener( OscServer::getInstance()->oscEvent, this, &UnitBehaviorOSC::onOSCMessage );
}

void UnitBehaviorOSC::onOSCMessage(ofxOscMessage &m)
{
    if ( !enabled ) return;

//    ofLog() << m.getAddress();
    if(m.getAddress() == OSC_PLAY) {
        if ( m.getNumArgs() >= 2 ) {
            float volume = 1.0f;
            float x = m.getArgAsFloat(0) * Sounds::getInstance()->getInitialWindowWidth();
            float y = m.getArgAsFloat(1) * Sounds::getInstance()->getInitialWindowHeight();

//            ofLog() << x << " " << y;

            oscPosition = ofVec2f(x, y);

            if (m.getNumArgs() >= 3 ) {
                volume = m.getArgAsFloat(2);
            }

            parent->mouseDragged(oscPosition, 0);
            oscCursorOpacity = 255;
        } else {
            ofLog() << "No arguments for OSC: " << m.getAddress();
        }
    } else if ( m.getAddress() == OSC_PLAY_X ) {
        if ( m.getNumArgs() >= 1 ) {
            float volume = 1.0f;
            float x = m.getArgAsFloat(0) * Sounds::getInstance()->getInitialWindowWidth();
            oscPosition = ofVec2f(x, oscPosition.y);

            if (m.getNumArgs() >= 2) {
                volume = m.getArgAsFloat(1);
            }

            parent->mouseDragged(oscPosition, 0);
            oscCursorOpacity = 255;
        } else {
            ofLog() << "No arguments for OSC: " << m.getAddress();
        }
    } else if ( m.getAddress() == OSC_PLAY_Y ) {
        if ( m.getNumArgs() >= 1 ) {
            float volume = 1.0f;
            float y = m.getArgAsFloat(0) * Sounds::getInstance()->getInitialWindowHeight();
            oscPosition = ofVec2f(oscPosition.x, y);

            if (m.getNumArgs() >= 2) {
                volume = m.getArgAsFloat(1);
            }

            parent->mouseDragged(oscPosition, 0);
            oscCursorOpacity = 255;
        }
    }
}

Json::Value UnitBehaviorOSC::save()
{
    Json::Value oscConfig = Json::Value( Json::objectValue );
    oscConfig["enabled"] = enabled;
    return oscConfig;
}

void UnitBehaviorOSC::load(Json::Value jsonData)
{
    if ( jsonData["enabled"] != Json::nullValue ) {
        enabled = jsonData["enabled"].asBool();
    }
}

void UnitBehaviorOSC::update()
{
    if ( oscCursorOpacity > 0 ) oscCursorOpacity -= 2;
}

void UnitBehaviorOSC::draw()
{
    if ( oscCursorOpacity < 1 ) return;

    ofNoFill();
    ofSetColor(255,255,255, oscCursorOpacity);
    ofDrawLine((oscPosition.x), (oscPosition.y - 4.5f), (oscPosition.x), (oscPosition.y + 4.5f));
    ofDrawLine((oscPosition.x - 4.5f), (oscPosition.y), (oscPosition.x + 4.5f), (oscPosition.y));
}

void UnitBehaviorOSC::drawGui()
{
    ImGui::Checkbox("Listen /play/xy OSC message", &enabled);
    if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_BEHAVIOR_OSC_LISTEN);
//    if ( enabled ) {
//        ImGui::InputText("Custom OSC address", customOSCPlay.c_str());
//    }
}
