#include "OscUnit.h"
#include "AudioEngine.h"

void OscUnit::oscPlay(float volume)
{
    oscCursorOpacity = 255;

    Sound * nearestSound = Sounds::getInstance()->getNearestSound( oscPosition );
    if ( nearestSound != nullptr ) {
        playSound( nearestSound, volume );
    }
}

OscUnit::OscUnit(){
    ofAddListener( OscServer::getInstance()->oscEvent, this, &OscUnit::onOSCMessage );
}

void OscUnit::onOSCMessage(ofxOscMessage &m)
{
    ofLog() << m.getAddress();
    if(m.getAddress() == OSC_PLAY) {
        if ( m.getNumArgs() >= 2 ) {
            float volume = 1.0f;
            float x = m.getArgAsFloat(0) * Sounds::getInstance()->getInitialWindowWidth();
            float y = m.getArgAsFloat(1) * Sounds::getInstance()->getInitialWindowHeight();

            ofLog() << x << " " << y;

            oscPosition = ofVec2f(x, y);

            if (m.getNumArgs() >= 3 ) {
                volume = m.getArgAsFloat(2);
            }

            oscPlay(volume);
        } else {
            ofLog() << "No arguments for OSC: " << m.getAddress();
        }
    } else if ( m.getAddress() == OSC_PLAY_X ) {
        if ( m.getNumArgs() >= 1 ) {
            float volume = 1.0f;
            float x = m.getArgAsFloat(0) * Sounds::getInstance()->getInitialWindowWidth();
            oscPosition = ofVec2f(x, oscPosition.y);

            if (m.getNumArgs() >= 2) {
                volume = m.getArgAsFloat(1);
            }

            oscPlay(volume);
        } else {
            ofLog() << "No arguments for OSC: " << m.getAddress();
        }
    } else if ( m.getAddress() == OSC_PLAY_Y ) {
        if ( m.getNumArgs() >= 1 ) {
            float volume = 1.0f;
            float y = m.getArgAsFloat(0) * Sounds::getInstance()->getInitialWindowHeight();
            oscPosition = ofVec2f(oscPosition.x, y);

            if (m.getNumArgs() >= 2) {
                volume = m.getArgAsFloat(1);
            }

            oscPlay(volume);
        }
    } else if ( m.getAddress() == OSC_PLAY_ID ) {
        if ( m.getNumArgs() >= 1 ) {

            float volume = 1.0f;

            if ( m.getNumArgs() >= 2 ) {
                volume = m.getArgAsFloat(1);
            }

            Sound * soundToPlay = Sounds::getInstance()->getSoundById( m.getArgAsInt(0) );
            if ( soundToPlay != NULL ) {
                playSound( soundToPlay, volume );
            } else {
                ofLog() << "ID not found: " << m.getAddress();
            }
        } else {
            ofLog() << "No arguments for OSC: " << m.getAddress();
        }
    } else if ( m.getAddress() == OSC_PLAY_CLUSTER_BY_SOUND_ID) {
        if ( m.getNumArgs() >= 1 ) {
            int id = m.getArgAsInt(0);
            Sound * s = Sounds::getInstance()->getSoundById( id );
            if ( s != NULL ) {
                if ( s->getCluster() > -1 ) {
                    vector<Sound *> cluster = Sounds::getInstance()->getSoundsByCluster( s->getCluster() );
                    Sound * soundToPlay = nullptr;

                    if ( m.getNumArgs() >= 2 ) {
                        int soundIdx = m.getArgAsInt(1);
                        soundIdx = soundIdx % cluster.size();
                        soundToPlay = cluster[soundIdx];
                    } else { //if index is not provided choose random
                        do {
                            soundToPlay = cluster[ floor( ofRandom(0, cluster.size()) ) ];
                        } while( soundToPlay == s );
                    }

                    float volume = 1.0f;

                    if ( m.getNumArgs() >= 3 ) {
                        volume = m.getArgAsFloat(2);
                    }

                    playSound( soundToPlay, volume );
                } else {
                    ofLog() << "Sound is not in a cluster for OSC: " << m.getAddress();
                }
            }

        } else {
            ofLog() << "No arguments for OSC: " << m.getAddress();
        }
    }

    else if(m.getAddress() == OSC_PLAY_CLUSTER) {
        vector<Sound *>snds;
        string clusterName = "";
        unsigned int sndIdx = 0;
        float volume = 0.9f;

        if(m.getNumArgs() > 0){
            clusterName = m.getArgAsString(0);
            snds = Sounds::getInstance()->getSoundsByCluster(clusterName);

            if ( snds.size() == 0 ) {
                ofLog() << "No sounds found in cluster for OSC: " << m.getAddress();
                return;
            }
        } else {
            ofLog() << "No arguments for OSC: " << m.getAddress();
            return;
        }
        //Sólo estoy mandando el cluster, elijo index randommente
        if(m.getNumArgs() == 1) {
            sndIdx = floor( ofRandom(0, snds.size()));
        }
        //Mando cluster e index
        if(m.getNumArgs() >= 2) {
            sndIdx = m.getArgAsInt(1);
            sndIdx = sndIdx % snds.size();
        }
        //Mando cluster index y volumen
        if(m.getNumArgs() == 3){
           volume = m.getArgAsFloat(2);
        }

        playSound(snds[sndIdx], volume);
    }
}

void OscUnit::update()
{
    if ( oscCursorOpacity > 0 ) oscCursorOpacity -= 2;
}

void OscUnit::draw()
{
    if ( oscCursorOpacity < 1 ) return;

    ofNoFill();
    ofSetColor(255,255,255, oscCursorOpacity);
    ofDrawLine((oscPosition.x), (oscPosition.y - 4.5f), (oscPosition.x), (oscPosition.y + 4.5f));
    ofDrawLine((oscPosition.x - 4.5f), (oscPosition.y), (oscPosition.x + 4.5f), (oscPosition.y));
}

Json::Value OscUnit::save()
{
    return Json::Value::null;
}

void OscUnit::load(Json::Value jsonData)
{

}


