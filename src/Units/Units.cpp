#include "Units.h"
#include "ExplorerUnit.h"
#include "SequenceUnit.h"
#include "ParticleUnit.h"
#include "OscUnit.h"
#include "../GUI/ExtraWidgets.h"

Units* Units::instance = nullptr;

Units::Units() {
    masterVolume.format = VOLUME_FORMAT;

    ofAddListener( AudioEngine::getInstance()->soundCardInit, this, &Units::onSoundCardInit );
}

void Units::selectUnit(shared_ptr<Unit> unit)
{
    if ( unit == selectedUnit ) return;

    for(unsigned int i = 0; i < currentUnits.size(); i++) {
        if ( currentUnits[i] == selectedUnit ) {
            currentUnits[i]->onUnselectedUnit();
            break;
        }
    }
    selectedUnit = unit;
    unit->onSelectedUnit();

}

void Units::setIndexesToUnits()
{
    for(unsigned int i = 0; i < currentUnits.size(); i++) {
        currentUnits[i]->setIndex(i);

        OscServer::getInstance()->mixerChangedVolume(i, currentUnits[i]->volume.get());
        OscServer::getInstance()->mixerChangedPan(i, currentUnits[i]->pan.get());
        OscServer::getInstance()->mixerChangedSolo(i, currentUnits[i]->isSolo());
        OscServer::getInstance()->mixerChangedMute(i, currentUnits[i]->isMuted());
    }
}

void Units::loadUnit(shared_ptr<Unit> unit, const Json::Value &jsonUnit)
{
    unit->load(jsonUnit["params"]);
    unit->effects.load(jsonUnit["effects"]);

    unit->volume.set( jsonUnit["volume"].asFloat() );
    unit->pan.set( jsonUnit["pan"].asFloat() );

    // was session saved with the same device that is selected?
    if ( SessionManager::getInstance()->sessionAudioDeviceName ==
         AudioEngine::getInstance()->getSelectedDeviceName() &&
         jsonUnit["output"].asInt() != 0 ) {

        unit->selectedOutput = jsonUnit["output"].asInt();
    }

    unit->loadBehaviors( jsonUnit["behaviors"] );

    currentUnits.push_back(unit);
}

void Units::beforeDraw() {
    for(unsigned int i = 0; i < currentUnits.size(); i++) {
        for ( auto *behavior : currentUnits[i]->behaviors ) {
            behavior->beforeDraw();
        }

        currentUnits[i]->beforeDraw();
    }
}

bool Units::needToShowConfig()
{
    return selectedUnit != nullptr;
}
void Units::draw() {
    for(unsigned int i = 0; i < currentUnits.size(); i++) {
        for ( auto *behavior : currentUnits[i]->behaviors ) {
            behavior->draw();
        }
        currentUnits[i]->draw();
    }
}

void Units::drawGui()
{
    for(unsigned int i = 0; i < currentUnits.size(); i++) {
        auto unit = currentUnits[i];
        bool selected = selectedUnit == unit;

        ImGui::BeginGroup();
        if (selected)
            ImGui::PushStyleColor(ImGuiCol_ChildBg,  UNITS_UNIT_BGCOLOR );

        ImGui::BeginChild(("##unit" + ofToString(i)).c_str(),
                          ImVec2(UNITS_WINDOW_WIDTH, UNITS_UNIT_HEIGHT),
                          true);

        if (selected)
            ImGui::Text(unit->getUnitName().c_str());
        else
            ImGui::TextDisabled(unit->getUnitName().c_str());

        ImGui::SameLine(ImGui::GetWindowWidth() - 41);
        
        //mute
        if ( unit->uberMute.draw() ) {
            OscServer::getInstance()->mixerChangedMute(i, unit->uberMute.get());
        }

        ImGui::SameLine(ImGui::GetWindowWidth() - 23);

       //solo
        if ( unit->uberSolo.draw() ) {
            OscServer::getInstance()->mixerChangedSolo(i, unit->uberSolo.get());
        }

        if ( unit->needsToProcessSolo ) {
            processSoloMute();
            unit->needsToProcessSolo = false;
        }

        ImGui::BeginGroup();
            float volumeWidth = UNITS_WINDOW_WIDTH * 0.86f;
            ImGui::PushItemWidth( volumeWidth );

            if ( unit->volume.draw() ) {
                OscServer::getInstance()->mixerChangedVolume(i, unit->volume.get());
            }

            if ( ImGui::IsItemClicked() ) {
                selectUnit(unit);
            }

            //outputs
            ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(8.f,0.6f));
//            ImGui::PushStyleVar(ImGuiStyleVar_Alpha, 0.7f);
            ImGui::PushItemWidth( UNITS_WINDOW_WIDTH * 0.4f );
//            ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(.8f, .8f, .8f, 1.f));
//            ImGui::PushStyleColor(ImGuiCol_FrameBg, ImVec4(1.f, .0f, .0f, 1.f));
            if ( unit->selectedOutput > -1 ) {
                ofxImGui::AddCombo( unit->selectedOutput, AudioEngine::getInstance()->getEnabledOutputs() );
                if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_SELECT_OUTPUT );
            } else {
                ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(.8f, .0f, .0f, 1.f));
                ImGui::Text("No out!");
                ImGui::PopStyleColor();
                if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_SELECT_OUTPUT_NO_OUT );
            }

            ImGui::PopStyleVar(1);
//            ImGui::PopStyleColor();

            //pan
            ImGui::SameLine( UNITS_WINDOW_WIDTH * 0.46f );
            ImGui::PushItemWidth( UNITS_WINDOW_WIDTH * 0.4f );

            if ( unit->pan.draw() ) {
              OscServer::getInstance()->mixerChangedPan(i, unit->pan.get());
            }
        ImGui::EndGroup();

        ImGui::SameLine( volumeWidth + 11.f);
        ExtraWidgets::VUMeter( unit->vuRMSL.meter_output(),
                               unit->vuPeakPreL.meter_output(),
                               ImVec2(UNITS_WINDOW_WIDTH*0.02f,38.f) );
        ImGui::SameLine(0.f, 1.f);
        ExtraWidgets::VUMeter( unit->vuRMSR.meter_output(),
                               unit->vuPeakPreR.meter_output(),
                               ImVec2(UNITS_WINDOW_WIDTH*0.02f,38.f) );

        ImGui::EndChild();

        if ( ImGui::IsItemClicked() ) {
            selectUnit(unit);
        }

        if (selected) {
            ImGui::PopStyleColor();
        }

        ImGui::EndGroup();
    }

    // Add unit
    if ( ImGui::SmallButton("+") ) {
        showAddUnitMenu = true;
    }
    if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_ADD );

    if ( showAddUnitMenu ) {
        ImGui::OpenPopup("addUnit");
        if (ImGui::BeginPopup("addUnit")) {
            if (ImGui::MenuItem("Explorer Unit")) {
                addUnit( shared_ptr<Unit>(new ExplorerUnit()) );
                showAddUnitMenu = false;
                ImGui::CloseCurrentPopup();
            }

            if (ImGui::MenuItem("Sequence Unit")) {
                addUnit( shared_ptr<Unit>(new SequenceUnit()) );
                showAddUnitMenu = false;
                ImGui::CloseCurrentPopup();
            }

            if (ImGui::MenuItem("Particle Unit")) {
                addUnit( shared_ptr<Unit>(new ParticleUnit()) );
                showAddUnitMenu = false;
                ImGui::CloseCurrentPopup();
            }

            if (ImGui::MenuItem("OSC Unit")) {
                addUnit( shared_ptr<Unit>(new OscUnit()) );
                showAddUnitMenu = false;
                ImGui::CloseCurrentPopup();
            }

            ImGui::EndPopup();
        }
    }
    ////////////////

    ImGui::SameLine();

    if ( ImGui::SmallButton("-") ) {
        removeUnit( selectedUnit );
    }
    if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_REMOVE );
}

void Units::update() {
    for(unsigned int i = 0; i < currentUnits.size(); i++) {
        for ( auto *behavior : currentUnits[i]->behaviors ) {
            behavior->update();
        }
        currentUnits[i]->update();
    }
}

void Units::reset()
{
    for(unsigned int i = 0; i < currentUnits.size(); i++) {
        currentUnits[i]->die();
    }
    currentUnits.clear();
}

void Units::drawSelectedUnitSettings(){
    selectedUnit->drawGui();
    drawBehaviors();
    drawEffectsSettings();
}

void Units::drawEffectsSettings()
{
    if ( ExtraWidgets::Header("Effects") ) {
        if ( ImGui::Button("+ Add") ) {
            showEffectList = true;
        }
        if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_EFFECT_ADD);
        ImGui::NewLine();
        selectedUnit->effects.drawGui();

        if (showEffectList) {
            ImGui::OpenPopup("addEffect");
            if (ImGui::BeginPopup("addEffect")) {
                if (ImGui::MenuItem("VAFilter")) {
                    VAFilterUI * effect = new VAFilterUI();
                    selectedUnit->effects.addEffect(effect);
                    showEffectList = false;
                    ImGui::CloseCurrentPopup();
                }
                if (ImGui::MenuItem("BasiVerb")) {
                    BasiVerbUI * effect = new BasiVerbUI();
                    selectedUnit->effects.addEffect(effect);
                    showEffectList = false;
                    ImGui::CloseCurrentPopup();
                }
                if (ImGui::MenuItem("Delay")) {
                    DelayUI * effect = new DelayUI();
                    selectedUnit->effects.addEffect(effect);
                    showEffectList = false;
                    ImGui::CloseCurrentPopup();
                }
                if (ImGui::MenuItem("DimensionChorus")) {
                    DimensionChorusUI * effect = new DimensionChorusUI();
                    selectedUnit->effects.addEffect(effect);
                    showEffectList = false;
                    ImGui::CloseCurrentPopup();
                }
                if (ImGui::MenuItem("Saturator")) {
                    SaturatorUI * effect = new SaturatorUI();
                    selectedUnit->effects.addEffect(effect);
                    showEffectList = false;
                    ImGui::CloseCurrentPopup();
                }

                ImGui::EndPopup();
            }
        }
    }
}

void Units::drawBehaviors()
{
    if ( !selectedUnit->behaviors.empty() ) {
        for ( auto * b : selectedUnit->behaviors ) {
            if ( b->drawGUISeparate ) {
                b->drawGui();
            }
        }
        if ( ExtraWidgets::Header("Extra", false) ) {
            for ( auto * b : selectedUnit->behaviors ) {
                if ( !b->drawGUISeparate ) {
                    b->drawGui();
                }
            }

            ImGui::NewLine();
        }
    }
}

void Units::drawMasterVolume()
{
    ImGuiWindowFlags window_flags = 0;
    window_flags |= ImGuiWindowFlags_NoTitleBar;
    window_flags |= ImGuiWindowFlags_NoMove;
    window_flags |= ImGuiWindowFlags_NoResize;
    window_flags |= ImGuiWindowFlags_NoCollapse;
    window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

    ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.1f, 0.1f, 0.1f, 0.00f));
    ImGui::PushStyleColor(ImGuiCol_Border, ImVec4(0.0f, 0.0f, 0.0f, 0.00f));

    auto mainSettings = ofxImGui::Settings();
    mainSettings.windowPos = ImVec2(ofGetWidth() - 148, -8);
    mainSettings.lockPosition = true;

    if ( ofxImGui::BeginWindow("masterVolume", mainSettings, window_flags) ) {
        ImGui::BringWindowToDisplayFront(ImGui::GetCurrentWindow());
//        ImGui::Text("Master");
//        ImGui::SameLine();

//        ImGui::SameLine( volumeWidth + 11.f);
//        ExtraWidgets::VUMeter( unit->vuRMSL.meter_output(),
//                               unit->vuPeakPreL.meter_output(),
//                               ImVec2(UNITS_WINDOW_WIDTH*0.02f,38.f) );
//        ImGui::SameLine(0.f, 1.f);
//        ExtraWidgets::VUMeter( unit->vuRMSR.meter_output(),
//                               unit->vuPeakPreR.meter_output(),
//                               ImVec2(UNITS_WINDOW_WIDTH*0.02f,38.f) );
        ImGui::PushItemWidth( 100.f );

        ImGui::PushStyleColor(ImGuiCol_FrameBg, ImVec4(.13f, .24f, .17f, 1.f));
        ImGui::PushStyleColor(ImGuiCol_FrameBgHovered, ImVec4(.17f, .35f, .24f, 1.f));
        ImGui::PushStyleColor(ImGuiCol_FrameBgActive, ImVec4(.13f, .49f, .32f, 1.f));

        ImGui::PushStyleColor(ImGuiCol_SliderGrab, ImVec4(.0f, .6f, .38f, 1.f));
        ImGui::PushStyleColor(ImGuiCol_SliderGrabActive, ImVec4(.0f, .67f, .42f, 1.f));
        masterVolume.draw();
        ImGui::PopStyleColor(5);
    }
    ImGui::PopStyleColor(2);
    ofxImGui::EndWindow(mainSettings);
}

Units *Units::getInstance()
{
    if(instance == nullptr){
        instance = new Units();
    }
    return instance;
}

void Units::initUnits() {
    if ( currentUnits.empty() ) {
        addUnit( shared_ptr<Unit>(new ExplorerUnit()) );
    }

    //seleccioná un modo inicial
    selectUnit(currentUnits[0]);
}

void Units::addUnit(shared_ptr<Unit> newUnit)
{
    currentUnits.push_back(newUnit);
    selectUnit(newUnit);
    setIndexesToUnits();
}

void Units::removeUnit(shared_ptr<Unit> unit)
{
    if ( unit != nullptr ) {
        std::vector<shared_ptr<Unit>>::iterator position =
                std::find(currentUnits.begin(), currentUnits.end(), unit);
        if (position != currentUnits.end())
            (*position)->die();
            currentUnits.erase(position);

        if ( !currentUnits.empty() ) {
            selectUnit(currentUnits[0]);
        }
        setIndexesToUnits();
    }
}

UberSlider * Units::getUnitVolume(unsigned int index)
{
    if ( index < currentUnits.size() ) {
        return &(currentUnits[index]->volume);
    }

    return nullptr;
}

UberSlider *Units::getUnitPan(unsigned int index)
{
    if ( index < currentUnits.size() ) {
        return &(currentUnits[index]->pan);
    }

    return nullptr;
}

UberToggle *Units::getUnitMute(unsigned int index)
{
    if ( index < currentUnits.size() ) {
        return &(currentUnits[index]->uberMute);
    }

    return nullptr;
}

UberToggle *Units::getUnitSolo(unsigned int index)
{
    if ( index < currentUnits.size() ) {
        return &(currentUnits[index]->uberSolo);
    }

    return nullptr;
}

void Units::setUnitVolume(unsigned int index, float v)
{
    currentUnits[index]->setVolume(v);
}

Json::Value Units::save()
{
    Json::Value root = Json::Value( Json::arrayValue );
    for(unsigned int i = 0; i < currentUnits.size(); i++) {
        Json::Value jsonUnit = Json::Value( Json::objectValue );
        jsonUnit["name"] = currentUnits[i]->getUnitName();
        jsonUnit["volume"] = currentUnits[i]->volume.get();
        jsonUnit["output"] = currentUnits[i]->selectedOutput.get();
        jsonUnit["pan"] = currentUnits[i]->pan.get();
        jsonUnit["params"] = currentUnits[i]->save();
        jsonUnit["effects"] = currentUnits[i]->effects.save();
        jsonUnit["behaviors"] = currentUnits[i]->saveBehaviors();

        root.append(jsonUnit);
    }
    return root;
}

void Units::load(Json::Value jsonUnits)
{
    for ( int i = 0 ; i < jsonUnits.size() ; i++ ) {
        Json::Value jsonUnit = jsonUnits[i];

        if ( jsonUnit["name"].asString() == "Explorer Unit" ) {
            loadUnit(shared_ptr<Unit>(new ExplorerUnit()), jsonUnit);
        }
        if ( jsonUnit["name"].asString() == "Sequence Unit" ) {
            loadUnit(shared_ptr<Unit>(new SequenceUnit()), jsonUnit);
        }
        if ( jsonUnit["name"].asString() == "Particle Unit" ) {
            loadUnit(shared_ptr<Unit>(new ParticleUnit()), jsonUnit);
        }
    }
    setIndexesToUnits();
}

void Units::mousePressed(int x, int y, int button) {

    //this happens only when a popup is opened but the mouse is not hovering it
    if (Gui::getInstance()->isMouseHoveringGUI() && !ImGui::IsAnyWindowHovered()) {
        showAddUnitMenu = false;
        showEffectList = false;
    } else {
        for ( auto *behavior : selectedUnit->behaviors ) {
            behavior->mousePressed( ofVec2f(x,y), button );
        }
        selectedUnit->mousePressed( ofVec2f(x,y), button );
    }
}

void Units::keyPressed(ofKeyEventArgs & e)
{
    for ( auto *behavior : selectedUnit->behaviors ) {
        behavior->keyPressed(e);
    }
    selectedUnit->keyPressed(e);
}

void Units::mouseDragged(int x, int y, int button){
    for ( auto *behavior : selectedUnit->behaviors ) {
        behavior->mouseDragged(ofVec2f(x,y), button);
    }
    selectedUnit->mouseDragged(ofVec2f(x,y), button);
}

void Units::mouseReleased(int x, int y , int button) {
    for ( auto *behavior : selectedUnit->behaviors ) {
        behavior->mouseReleased(ofVec2f(x,y));
    }
    selectedUnit->mouseReleased(ofVec2f(x,y));
}

void Units::mouseMoved(int x, int y) {
    for ( auto *behavior : selectedUnit->behaviors ) {
        behavior->mouseMoved(ofVec2f(x,y));
    }
    selectedUnit->mouseMoved(ofVec2f(x,y));
}

void Units::midiMessage(MIDIMessage m) {
    //Si está en midiLearn sólo le manda mensaje al modo activo
    if(MidiServer::getInstance()->getMIDILearn()){
        selectedUnit->midiMessage(m);
    }else{
        for(unsigned int i = 0; i < currentUnits.size(); i++) {
            currentUnits[i]->midiMessage(m);
        }
    }
}

shared_ptr<Unit> Units::getSelectedUnit() {
    return selectedUnit;
}

string Units::getSelectedUnitName() {
    return selectedUnit->getUnitName();
}

void Units::processSoloMute() {
    bool isAnyUnitSoloed = false;
    for (unsigned int i = 0; i < currentUnits.size(); i++) {
        if ( currentUnits[i]->isSolo() ) {
            isAnyUnitSoloed = true;
            break;
        }
    }
    if ( isAnyUnitSoloed ) {
        for(unsigned int i = 0; i < currentUnits.size(); i++) {
            auto unit = currentUnits[i];
            unit->setMute(!unit->isSolo(), true);
        }
    } else {
        for(unsigned int i = 0; i < currentUnits.size(); i++) {
            auto unit = currentUnits[i];
            unit->setMute(false, true);
        }
    }
}

void Units::onSoundCardInit(int &deviceID)
{
    masterGain.disconnectOut();
    unsigned int countOutputs = AudioEngine::getInstance()->getAudioOutNumChannels(deviceID);
    for ( unsigned int i = 0 ; i < countOutputs ; i++ ) {
        masterGain.ch(i) >> AudioEngine::getInstance()->engine.audio_out(i);
    }

    //Soundcard has changed for one with less outputs. Unit has a selected output that exists no more.
    for (unsigned int i = 0; i < currentUnits.size(); i++) {
        if ( currentUnits[i]->selectedOutput >= countOutputs/2 ) {
            currentUnits[i]->selectedOutput = 0;
        }
    }
}
