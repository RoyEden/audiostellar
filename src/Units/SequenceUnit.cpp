#include "SequenceUnit.h"
#include "../GUI/ExtraWidgets.h"
#include "Behaviors/UnitBehaviorDragToPlay.h"

int SequenceUnit::intBars[QTY_BARS] = {QTY_STEPS/4, QTY_STEPS /2, QTY_STEPS, QTY_STEPS*2, QTY_STEPS*3, QTY_STEPS*4};

SequenceUnit::SequenceUnit() {
    parameters.push_back(&probability);
    parameters.push_back(&offset);

    offset.format = "%.0f";
    offset.addListener(this, &SequenceUnit::onNeedToProcess);
    selectedBars.addListener(this, &SequenceUnit::onNeedToProcessInt);

    ofAddListener( MasterClock::getInstance()->onTempo, this, &SequenceUnit::onTempo );

    behaviors.push_back( new UnitBehaviorDragToPlay(this) );
}

void SequenceUnit::setIndexExtra(int i)
{
    string name = "Bars##Unit" + ofToString(i);
    selectedBars.setName(name);
//    name = "Offset##Unit" + ofToString(i);
//    offset.setName(name);
}

void SequenceUnit::onNeedToProcess(float &a){
    processSequence();
}
void SequenceUnit::onNeedToProcessInt(int &a){
    processSequence();
}

Json::Value SequenceUnit::save() {
    Json::Value sequence = Json::Value( Json::objectValue );
    Json::Value ids = Json::Value( Json::arrayValue );

    for ( unsigned int j = 0 ; j < secuencia.size() ; j++ ) {
        ids.append( secuencia[j]->id );
    }

    sequence["ids"] = ids;
    sequence["offset"] = offset.get();
    sequence["probability"] = probability.get();
    sequence["bars"] = selectedBars.get();

    return sequence;
}
void SequenceUnit::load(Json::Value jsonData) {
    if ( jsonData["ids"] != Json::nullValue ) {
        Json::Value ids = jsonData["ids"];
        for ( unsigned int i = 0 ; i < ids.size() ; i++ ) {
            toggleSound( Sounds::getInstance()->getSoundById( ids[i].asInt() ) , false);
        }
    }

    if ( jsonData["offset"] != Json::nullValue ) offset.set( jsonData["offset"].asInt() );
    if ( jsonData["probability"] != Json::nullValue ) probability.set( jsonData["probability"].asFloat() );
    if ( jsonData["bars"] != Json::nullValue ) selectedBars.set( jsonData["bars"].asInt() );

    processSequence();
    setSelectAllSounds(false);
}

void SequenceUnit::onSelectedUnit() {
    Unit::onSelectedUnit();
    setSelectAllSounds();
}
void SequenceUnit::onUnselectedUnit() {
    Unit::onUnselectedUnit();
    setSelectAllSounds(false);
}

void SequenceUnit::setSelectAllSounds(bool selected)
{
    for ( unsigned int i = 0 ; i < secuencia.size() ; i++ ) {
        secuencia[i]->selected = selected;
    }
}

void SequenceUnit::update() {

}

void SequenceUnit::draw() {
//tss look beforeDraw
}

void SequenceUnit::drawGui()
{
    if ( ExtraWidgets::Header("Parameters") ) {
//        ofxImGui::AddParameter( offset );
//        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::UNIT_SEQUENCE_OFFSET);
        offset.draw();

        probability.draw();

        ofxImGui::AddCombo(selectedBars, strBars);
        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::UNIT_SEQUENCE_BARS);

        if ( secuencia.size() > 0 ) {
            ImGui::NewLine();
            if ( ImGui::Button("Clear sequence") ) {
                clearSequence();
            }
            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::UNIT_SEQUENCE_CLEAR);
        }

        ImGui::NewLine();
    }
}

void SequenceUnit::onTempo(int & frame)
{
    if ( isProcessing ) return;
//    if ( !MasterClock::getInstance()->isPlaying() ) return;

    currentStep = frame % getCantSteps();

    if ( secuenciaEnTiempo != nullptr && secuenciaEnTiempo[currentStep] != nullptr ) {
        if ( ofRandom(1.f) <= probability.get() ) {
            playSound(secuenciaEnTiempo[currentStep]);
        }
    }
}

void SequenceUnit::processSequence()
{
    isProcessing = true;
    polyLine.clear();

    secuenciaEnTiempo = new Sound*[getCantSteps()];

    for ( int i = 0 ; i < getCantSteps() ; i++ ) {
        secuenciaEnTiempo[i] = NULL;
    }

    if ( secuencia.size() == 0 ) {
        isProcessing = false;
        return;
    }

    unsigned int secuenciaSize = secuencia.size();
    if ( getCantSteps() < secuencia.size() ) {
        secuenciaSize = getCantSteps();
    }

    // saco distancia total
    vector<float> distancias;
    float distanciaAcumulada = 0;
    for ( unsigned int i = 1 ; i < secuenciaSize ; i++ ) {
        float distancia = Utils::distance( secuencia[i-1]->getPosition(), secuencia[i]->getPosition() );
        distancias.push_back(distancia);
        distanciaAcumulada += distancia;
    }

    // if ( secuencia.size() == 2 ) {
        float distanciaUltimoConPrimero = Utils::distance( secuencia[secuencia.size()-1]->getPosition(), secuencia[0]->getPosition() );
        distanciaAcumulada += distanciaUltimoConPrimero;
    // }

//     cout << "Distancias iniciales: ";
//     for ( unsigned int i = 0 ; i < distancias.size() ; i++ ) {
//         cout << distancias[i] << ",";
//     }
//     cout << endl;

    secuenciaEnTiempo[0] = secuencia[0];

    int stepsAcumulados = 0;

    for ( unsigned int i = 0 ; i < distancias.size() ; i++ ) {
        distancias[i] = distancias[i] / distanciaAcumulada;
        int relacionDistanciaSteps = floor(distancias[i] * getCantSteps()); //aca es en relacion con cantSteps no QTY_STEPS

        if (relacionDistanciaSteps == 0) {
            relacionDistanciaSteps = 1;
        }

        distancias[i] = relacionDistanciaSteps + stepsAcumulados;

        stepsAcumulados += relacionDistanciaSteps;
    }

//     cout << "Distancias finales: ";
    for ( unsigned int i = 0 ; i < distancias.size() ; i++ ) {
//         cout << distancias[i] << ",";
        int step = distancias[i];
        secuenciaEnTiempo[ step ] = secuencia[i+1];
    }
//     cout << endl;

//    cout << "secuenciaEnTiempo: [";
//    for ( unsigned int i = 0 ; i < cantSteps ; i++ ) {
//        if ( secuenciaEnTiempo[i] == NULL ) {
//            cout << "0 ";
//        } else {
//            cout << "1 ";
//        }

//    }
//    cout << "]" << endl;

    for ( unsigned int i = 0 ; i < secuenciaSize ; i++ ) {
        polyLine.addVertex( secuencia[i]->getPosition().x,
                            secuencia[i]->getPosition().y, 0 );
    }

    polyLine.close();

    processSequenceOffset();

    isProcessing = false;
}

void SequenceUnit::processSequenceOffset()
{
    Sound ** secuenciaEnTiempoOffset = new Sound*[getCantSteps()];

    for ( int i = 0 ; i < getCantSteps() ; i++ ) {
        secuenciaEnTiempoOffset[ (i + int(offset.get()) ) % getCantSteps() ] = secuenciaEnTiempo[i];
    }

    secuenciaEnTiempo = secuenciaEnTiempoOffset;

//    cout << "secuenciaEnTiempoOffset: [";
//    for ( unsigned int i = 0 ; i < getCantSteps() ; i++ ) {
//        if ( secuenciaEnTiempoOffset[i] == NULL ) {
//            cout << "0 ";
//        } else {
//            cout << "1 ";
//        }

//    }
//    cout << "]" << endl;
}

void SequenceUnit::clearSequence()
{
    setSelectAllSounds(false);
    secuencia.clear();

    for ( int i = 0 ; i < getCantSteps() ; i++ ) {
        secuenciaEnTiempo[i] = NULL;
    }

    polyLine.clear();
    reset();
}

int SequenceUnit::getCantSteps()
{
    return intBars[selectedBars];
}

void SequenceUnit::toggleSound(Sound *sound, bool doProcessSequence)
{
    if ( sound != NULL ) {
        if ( !sound->selected ) {
            //TODO : infinite sequences
            if ( secuencia.size() < getCantSteps() ) {
                secuencia.push_back( sound );
                sound->selected = true;
            }
        } else {
            secuencia.erase(
                std::remove(secuencia.begin(),
                secuencia.end(), sound), secuencia.end());

            sound->selected = false;

        }

        // ofLog() << secuencia.size() << endl;
        if ( doProcessSequence ) {
            processSequence();
        }
    }
}



void SequenceUnit::beforeDraw() {
    if ( isProcessing ) {
        return;
    }

    ofSetLineWidth(1);

    if ( selected ) {
        ofSetColor(ofColor(255,255,255));
    } else {
        ofSetColor(ofColor(80,80,80));
    }

    polyLine.draw();

    if ( playing ) {
        ofSetColor(ofColor(255,255,255));
        ofFill();

        if ( secuencia.size() > 0 ) {
            ofPoint p;

            if ( secuenciaEnTiempo[currentStep] != NULL ) {
//                p = secuenciaEnTiempo[currentStep]->getPosition();
            } else {
                float offsetedStep = ( currentStep + ( getCantSteps() - int(offset.get()) ) ) % getCantSteps();
                p = polyLine.getPointAtPercent( offsetedStep / getCantSteps() );
            }
            if ( !(p.x == 0.0f && p.y == 0.0f)) {
                ofDrawEllipse( p.x , p.y, 5, 5 );
            }
        }
    }
}

void SequenceUnit::mousePressed(ofVec2f p, int button) {
    Sound * hoveredSound = Sounds::getInstance()->getHoveredSound();
    if ( hoveredSound != nullptr ) {
        if ( button == 0 ) {
            toggleSound( hoveredSound, true );
        }
    }
}

void SequenceUnit::mouseDragged(ofVec2f p, int button) {

}

void SequenceUnit::reset()
{
    probability.set(1.f);
    offset.set(0);
}

void SequenceUnit::die()
{
    ofRemoveListener( MasterClock::getInstance()->onTempo, this, &SequenceUnit::onTempo );
    setSelectAllSounds(false);
};
