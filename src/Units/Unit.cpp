#include "Unit.h"

Unit::Unit() {
    pan.format = "L | R";
    pan.small = true;

    volume.addListener(this, &Unit::volumeListener);
    volume.set(DEFAULT_VOLUME);
    volume.format = VOLUME_FORMAT;

    pan.addListener(this, &Unit::panListener);
    pan.set(0.5f);

    uberMute.addListener(this, &Unit::muteListener);
    uberMute.color = uberMute.COLOR_MUTE;
    uberMute.small = true;

    uberSolo.addListener(this, &Unit::soloListener);
    uberSolo.color = uberSolo.COLOR_SOLO;
    uberSolo.small = true;

    parameters.push_back(&volume);
    parameters.push_back(&pan);
    parameters.push_back(&uberMute);
    parameters.push_back(&uberSolo);

    selectedOutput.addListener(this, &Unit::selectedOutputListener);

    patch();
}

Unit::~Unit() {
    unpatch();

    for ( UnitBehavior * b : behaviors ) {
        delete b;
    }
};

void Unit::playSound(Sound *s, float volume) {
    if ( !s->mute && !effects.busy ) {
        Voice * v = s->play();
        setupVoice(v);

        v->setVolume(volume);
        //TODO: speed, envelope, spread

        v->out("0") >> effects.in("0");
        v->out("1") >> effects.in("1");

        //0.1 >> v->in("speed");
    } else if ( effects.busy ) {
        //I've never saw this log, probably needs a rewrite
        ofLog() << "effects is busy";
    }
}

void Unit::setupVoice(Voice *v){
    //Disconnect voice if it was previously being used by other unit
    std::vector<pdsp::InputNode*> outs = v->out("0").getSelectedOutput().getOutputs();

    for(unsigned int i = 0; i < outs.size(); i++){
        if(outs[i] != &this->in("0").getSelectedInput()){
            v->out("0").getSelectedOutput().disconnect(*outs[i]);
        }
    }

    outs = v->out("1").getSelectedOutput().getOutputs();

    for(unsigned int i = 0; i < outs.size(); i++){
        if(outs[i] != &this->in("1").getSelectedInput()){
            v->out("1").getSelectedOutput().disconnect(*outs[i]);
        }
    }
}

void Unit::volumeListener(float &v) {
    if (volume.get() == volume.getMin()) {
        volume.format = "-inf";
    } else {
        volume.format = VOLUME_FORMAT;
    }
    updateVolume();
}

void Unit::panListener(float &v) {
    panL = ofClamp((1.0f - v)*2, 0.f,1.0f);
    panR = ofClamp(v*2, 0.f,1.0f);
    updateVolume();
}

void Unit::updateVolume() {
    if ( mute || forceMute || volume.get() == volume.getMin() ) {
        0.0001f >> outL.in_mod();
        0.0001f >> outR.in_mod();
    } else {
        panL >> outL.in_mod();
        panR >> outR.in_mod();
    }
}

void Unit::muteListener(float &v) {
    setMute( uberMute.isActive() );
}

void Unit::soloListener(float &v) {
    setSolo( uberSolo.isActive() );
}

void Unit::setVolume(float v)
{
    volume.set( ofMap(v, 0, 1, volume.getMin(), volume.getMax()) );
    updateVolume();
}

void Unit::setMute(bool mute, bool forced) {
    if ( !forced ) {
        this->mute = mute;
    } else {
        this->forceMute = mute;
    }
    needsToProcessSolo = true;
    updateVolume();
}

void Unit::setSolo(bool v) {
    solo = v;
    needsToProcessSolo = true;
}

void Unit::selectedOutputListener(int &v)
{
    AudioEngine::Output selectedOutputObj = AudioEngine::getInstance()->getEnabledOutput(v);

    if ( selectedOutputObj.label != "error" ) {
        outL.disconnectOut();
        outR.disconnectOut();
//        outL >> AudioEngine::getInstance()->engine.audio_out( selectedOutputObj.channel0 );
//        outR >> AudioEngine::getInstance()->engine.audio_out( selectedOutputObj.channel1 );
        outL >> Units::getInstance()->masterGain.ch( selectedOutputObj.channel0 );
        outR >> Units::getInstance()->masterGain.ch( selectedOutputObj.channel1 );

        patchVUmeter();
    } else {
        ofLog() << "Tried to set an audio output for unit that is not enabled from audio settings";
        selectedOutput.set(0);
    }

}

void Unit::patchVUmeter()
{
    outL >> vuRMSL >> AudioEngine::getInstance()->engine.blackhole();
    outR >> vuRMSR >> AudioEngine::getInstance()->engine.blackhole();
    outL >> vuPeakPreL >> AudioEngine::getInstance()->engine.blackhole();
    outR >> vuPeakPreR >> AudioEngine::getInstance()->engine.blackhole();
}

void Unit::setIndex(int i) {
    string name = "Unit" + ofToString(i);
    effects.setIndexesToEffects(name);
    name = "##" + name;

    for ( unsigned int j = 0 ; j < parameters.size() ; j++ ) {
        parameters[j]->setName(parameters[j]->originalName + name);
    }

    setIndexExtra(i);
}

Json::Value Unit::saveBehaviors() {
    Json::Value root = Json::Value( Json::arrayValue );
    for(unsigned int i = 0; i < behaviors.size(); i++) {
        UnitBehavior * behavior = behaviors[i];
        Json::Value jsonUnit = Json::Value( Json::objectValue );
        jsonUnit["name"] = behavior->getName();
        jsonUnit["params"] = behavior->save();
        root.append(jsonUnit);
    }
    return root;
}

void Unit::loadBehaviors( Json::Value jsonData )
{
    if ( jsonData != Json::nullValue ) {
        for ( UnitBehavior * b : behaviors ) {
            for ( unsigned int i = 0 ; i < jsonData.size() ; i++ ) {
                if ( jsonData[i]["name"] == b->getName() ) {
                    b->load(jsonData[i]["params"]);
                    break;
                }
            }
        }
    }
}

void Unit::patch(){
    addModuleInput("0", effects.in("0"));
    addModuleInput("1", effects.in("1"));

    effects.out("0") >> outGain.ch(0) >> outL;
    effects.out("1") >> outGain.ch(1) >> outR;

//    outL >> AudioEngine::getInstance()->engine.audio_out(0);
//    outR >> AudioEngine::getInstance()->engine.audio_out(1);

    outL >> Units::getInstance()->masterGain.ch(0) ;
    outR >> Units::getInstance()->masterGain.ch(1) ;

    patchVUmeter();
}

void Unit::unpatch() {
    outL.disconnectAll();
    outR.disconnectAll();
    outGain.disconnectAll();
    vuRMSL.disconnectAll();
    vuRMSR.disconnectAll();
    vuPeakPreL.disconnectAll();
    vuPeakPreR.disconnectAll();
}
