#include "ofApp.h"
#include "Voices.h"
//--------------------------------------------------------------
void ofApp::setup() {
    #ifdef TARGET_OS_MAC
        ofSetDataPathRoot("../Resources/data/");
    #endif
    
    ofSetWindowTitle("AudioStellar");
    ofSetEscapeQuitsApp(false);

    ofEnableAlphaBlending();
    ofSetVerticalSync(true);

    sessionManager = SessionManager::getInstance();
    sounds = Sounds::getInstance();
    midiServer = MidiServer::getInstance();
    oscServer = OscServer::getInstance();

    units = Units::getInstance();
    gui = Gui::getInstance();

    midiServer->init();
    cam.init();
    
    Voices::getInstance()->setup();

    MasterClock::getInstance(); //init

    sessionManager->loadInitSession();
}

//--------------------------------------------------------------
void ofApp::update() {
    sounds->update();
    units->update();
    oscServer->update();
}

//--------------------------------------------------------------
void ofApp::draw() {
    ofBackground(25);

    cam.begin();
        units->beforeDraw();
        sounds->draw();
        units->draw();
    cam.end();

    gui->draw();
}

void ofApp::keyPressed(ofKeyEventArgs & e) {
    gui->keyPressed(e);
    units->keyPressed(e);
    sessionManager->keyPressed(e);
    sounds->keyPressed(e);
}

void ofApp::mousePressed(int x, int y, int button) {
    ofVec3f realCoordinates = cam.screenToWorld(ofVec3f(x,y,0));
    if(!gui->isMouseHoveringGUI()){
        units->mousePressed( realCoordinates.x,
                             realCoordinates.y,
                             button );
        sounds->mousePressed(realCoordinates.x,
                             realCoordinates.y,
                             false);
    }
    gui->mousePressed(realCoordinates, button);
}

void ofApp::mouseMoved(int x, int y) {
    if(sessionManager->getSessionLoaded()){
        if(!gui->isMouseHoveringGUI()){
            ofVec3f realCoordinates = cam.screenToWorld(ofVec3f(x,y,0));
            sounds->mouseMoved(realCoordinates.x,
                               realCoordinates.y,
                               false);
        }
    }
}

void ofApp::mouseDragged(int x, int y, int button) {
    if(!gui->isMouseHoveringGUI()){
        ofVec3f realCoordinates = cam.screenToWorld(ofVec3f(x,y,0));
        units->mouseDragged(realCoordinates.x,
                            realCoordinates.y,
                            button);

        sounds->mouseDragged(realCoordinates, button);
    }

}

void ofApp::mouseReleased(int x, int y, int button) {
    ofVec3f realCoordinates = cam.screenToWorld(ofVec3f(x,y,0));

    //changing this order will cause sound context menu to be shown after sound moved
    gui->mouseReleased(realCoordinates, button);

    if(!gui->isMouseHoveringGUI()){
        units->mouseReleased(realCoordinates.x,
                             realCoordinates.y,
                             button);
        sounds->mouseReleased(realCoordinates.x,
                             realCoordinates.y,
                             button);
    }
}

void ofApp::keyReleased(int key) {
    sounds->keyReleased(key);

}
void ofApp::mouseScrolled(int x, int y, float scrollX, float scrollY) {
    // cam.mouseScrolled(x,y,scrollX,scrollY);
}

void ofApp::exit(){
    sessionManager->exit();
}



