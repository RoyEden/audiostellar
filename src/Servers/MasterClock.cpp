#include "MasterClock.h"

MasterClock* MasterClock::instance = nullptr;

MasterClock::MasterClock()
{
    bpm.setName("BPM");
    bpm.setMin(20);
    bpm.setMax(300);
    bpm.addListener(this, &MasterClock::bpmListener);
    bpm = 120;

    initPDSPFunctionClock();
}

MasterClock *MasterClock::getInstance()
{
    if ( instance == nullptr ) {
        instance = new MasterClock();
    }
    return instance;
}

void MasterClock::setUseMidiClock(bool v)
{
    useMIDIClock = v;

    if ( useMIDIClock ) {
        destroyPDSPFunctionClock();
    } else {
        initPDSPFunctionClock();
    }
}

bool MasterClock::getUseMidiClock()
{
    return useMIDIClock;
}

void MasterClock::onMIDIClockTick(MIDIMessage m)
{
    if (!useMIDIClock) return;
    if (!playing) return;
    ofNotifyEvent(onTempo, m.beats);
}

void MasterClock::initPDSPFunctionClock()
{
    pdspFunction = new pdsp::Function();
    // this assignable function is executed each 16th
    pdspFunction->code = [&]() noexcept {
        int frame = pdspFunction->frame();
        if ( playing ) {
            ofNotifyEvent(onTempo, frame);
        }
    };
}

void MasterClock::destroyPDSPFunctionClock()
{
    if ( pdspFunction != nullptr ) {
        delete pdspFunction;
        pdspFunction = nullptr;
    }
}

void MasterClock::drawGui()
{
    ImGui::PushItemWidth( 100.f );
    if ( !useMIDIClock ) {
        ofxImGui::AddParameter( bpm );
        //Gui::getInstance()->uberSlider( bpm ); //int needed
    } else {
        ImGui::Button("MIDI Clock");
    }
}

void MasterClock::play()
{
    playing = true;
}

void MasterClock::stop()
{
    playing = false;
}

bool MasterClock::isPlaying()
{
    return playing;
}

void MasterClock::bpmListener(int &v) {
    AudioEngine::getInstance()->engine.sequencer.setTempo(v);
}
