#include "DelayUI.h"
#include "../GUI/UberSlider.h"

DelayUI::DelayUI()
{
    delayL.setMaxTime(MAX_DELAY_TIME);
    delayR.setMaxTime(MAX_DELAY_TIME);

    parameters.push_back( new UberSlider(time.set("Time", 100.f, 1.f, MAX_DELAY_TIME), &Tooltip::NONE) );
    parameters.push_back( new UberSlider(feedback.set("Feedback", 0.2f, 0.f, 1.3f), &Tooltip::NONE) );
    parameters.push_back( new UberSlider(damping.set("Damping", 0.5f, 0.f, 0.99f), &Tooltip::NONE) );

    inL >> delayL >> outL;
    inR >> delayR >> outR;

    time >> delayL.in_time();
    time >> delayR.in_time();
    feedback >> delayL.in_feedback();
    feedback >> delayR.in_feedback();
    damping >> delayL.in_damping();
    damping >> delayR.in_damping();
}

string DelayUI::getName()
{
    return "Delay";
}

pdsp::Patchable &DelayUI::channelIn(size_t index)
{
    if ( index == 0 ) {
        return inL;
    } else if ( index == 1 ) {
        return inR;
    } else {
        throw "DelayUI has stereo input only";
    }
}

pdsp::Patchable &DelayUI::channelOut(size_t index)
{
    if ( index == 0 ) {
        return outL;
    } else if ( index == 1 ) {
        return outR;
    } else {
        throw "DelayUI has stereo output only";
    }
}
