#pragma once

#include "ofMain.h"
#include "ofxPDSP.h"
#include "ofxImGui.h"
#include "ofxJSON.h"

#include "./EffectUI.h"

class EffectUI;

class AudioEffects : public pdsp::Patchable
{
private:
    vector<EffectUI *> effects; //should this be shared ?
    void patch();

    pdsp::PatchNode ampIn0;
    pdsp::PatchNode ampIn1;
    pdsp::PatchNode ampOut0;
    pdsp::PatchNode ampOut1;



    string indexPrefix;

public:
    AudioEffects();
    ~AudioEffects();

    bool busy = false;

    void addEffect( EffectUI * newEffect );
    void removeEffect( EffectUI * effect );

    Json::Value save();
    void load(Json::Value jsonAudioEffects);
    void setIndexesToEffects(string indexPrefix);

    void drawGui();
};
