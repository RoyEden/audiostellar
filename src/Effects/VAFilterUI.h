#pragma once

#include "ofMain.h"
#include "ofxPDSP.h"
#include "ofxImGui.h"

#include "./EffectUI.h"

class VAFilterUI : public EffectUI, public pdsp::VAFilter {
private:
    pdsp::Parameter cutoff;
    pdsp::Parameter resonance;

    const char* mode_items[6] = { "LowPass24",
                                  "LowPass12",
                                  "HighPass24",
                                  "HighPass12",
                                  "BandPass24",
                                  "BandPass12" };
    float mode_values[6] = { LowPass24, LowPass12, HighPass24, HighPass12, BandPass24, BandPass12 };
    ofParameter<int> mode_current = 0;

    void parseLoadedParams();
public:
    VAFilterUI();

    string getName();

    Json::Value save();
    void setIndex(int i, string indexPrefix);

    pdsp::Patchable & channelIn(size_t index);
    pdsp::Patchable & channelOut(size_t index);
    void drawGui();
};
