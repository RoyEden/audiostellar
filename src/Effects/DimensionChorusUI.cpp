#include "DimensionChorusUI.h"


DimensionChorusUI::DimensionChorusUI()
{
    parameters.push_back( new UberSlider(speed.set("Speed",0.25f,0.f,5.f), &Tooltip::NONE) );
    parameters.push_back( new UberSlider(depth.set("Depth",10.f,0.f,1000.f), &Tooltip::NONE) );
    parameters.push_back( new UberSlider(delay.set("Delay",80.f,0.f,1000.f), &Tooltip::NONE) );

    speed >> in_speed();
    depth >> in_depth();
    delay >> in_delay();
}

string DimensionChorusUI::getName()
{
    return "DimensionChorus";
}

pdsp::Patchable &DimensionChorusUI::channelIn(size_t index)
{
    return ch(index);
}

pdsp::Patchable &DimensionChorusUI::channelOut(size_t index)
{
    return ch(index);
}
