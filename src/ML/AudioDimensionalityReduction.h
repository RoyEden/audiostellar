#pragma once
#include "ofMain.h"
#include "ofxJSON.h"

class AudioDimensionalityReduction : public ofThread
{
public:
    static string currentProcess;
    static float progress;
    static int filesFound;
    static string generatedJSONPath;
    static vector<string> failFiles;

    static string dirPath;

    static int targetSampleRate;
    static float targetDuration; // Seconds. Audios exceeding duration will be chopped, otherwise will be zero padded
    static int stft_windowSize;
    static int stft_hopSize;

    static string viz_method;
    static int tsne_perplexity;
    static int tsne_learning_rate;
    static int tsne_iterations;
    static int umap_n_neighbors;
    static float umap_min_dist;
    static string metric;

    static bool stft;
    static bool mfcc;
    static bool spectralCentroid;
    static bool chromagram;
    static bool rms;
    
    static bool pca_results;
    static bool force_full_process;
    
    
    /**
     * @brief run Runs the entire pipeline.
     *
     */
    static void run();
    
    static void checkProcess();
    static void end();

    void threadedFunction();
    
private:
    //on linux this will be the config directory for AppImage to work
    static string FILENAME_PROCESS_INFO;
};
