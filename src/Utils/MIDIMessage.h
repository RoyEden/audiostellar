#pragma once

#include "ofMain.h"

struct MIDIMessage {
  string status;
  int pitch;
  int cc;
  int ccVal;
  int beats = -1;
  double bpm = -1;
};
