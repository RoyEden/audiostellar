#pragma once
#include "ofMain.h"

class Utils {
public:
    static const int DISTANCE_TRESHOLD = 5;

    static float distance(ofVec2f p1, ofVec2f p2) {
        ofVec2f diff = p1 - p2;
        float dist = diff.length();
        return dist;
    }
    static float getMaxVal(float val, float winner) {
        if(val > winner) {
            return val;
        } else {
            return winner;
        }
    }
    static float getMinVal(float val, float winner) {
        if(val < winner) {
            return val;
        } else {
            return winner;
        }
    }

    static string resolvePath(string root, string path){

        if (!ofFilePath::isAbsolute(path)) {
            root = ofFilePath::getEnclosingDirectory(root);
            path = root + path;
        }

        if(path[path.size() - 1] != '/'){
            path = path + "/";
        }

        return path;
    }
};
