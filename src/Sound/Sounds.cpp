#include "Sounds.h"
#include "Voices.h"
#include "../Units/Units.h"
#include "../Units/SequenceUnit.h"

Sounds* Sounds::instance = nullptr;

Sounds* Sounds::getInstance(){
    if( instance == nullptr){
        instance = new Sounds();
    }
    
    return instance;
}


Sounds::Sounds() {
    useOriginalPositions = false;
    epsDbScan = 15;

    windowOriginalWidth = ofGetWidth();
    windowOriginalHeight = ofGetHeight();

    mainQuadTree = Quad(ofVec2f(0,0), ofVec2f(ofGetWidth(), ofGetHeight()));

    font.load( OF_TTF_MONO, 10 );
}

void Sounds::loadSounds(ofxJSONElement jsonFile){
    
    ofFile fileCheck;
    unsigned int filesDontExist = 0;

    if(!sounds.empty()){
        reset();
    }
    
    string soundsDir = jsonFile["audioFilesPathAbsolute"].asString();
    
    vector<string> soundFiles = ofSplitString(jsonFile["tsv"].asString(), "|");
    
    setSpaceLimits(soundFiles);
    
    for(int i = 0; i<soundFiles.size() - 1; i++) {
        if(soundFiles[i] == "")continue;
        vector<string>lineElements = ofSplitString(soundFiles[i],",");
        
        int cluster = ofToInt(lineElements[TSV_FIELD_CLUSTER]);
        string name = lineElements[TSV_FIELD_NAME];

        ofVec3f position = soundToCamCoordinates( ofVec3f(
                                                          ofToFloat(lineElements[TSV_FIELD_X]),
                                                          ofToFloat(lineElements[TSV_FIELD_Y]),
                                                          ofToFloat(lineElements[TSV_FIELD_Z])) );
        
        string file = soundsDir + name;
        
        Sound *sound = new Sound(file, position, cluster);

        if ( !fileCheck.doesFileExist(file, false) ) {
            filesDontExist++;
        }

        mainQuadTree.insert(sound);

        sound->id = idCount;
        idCount++;
        
        sounds.push_back(sound);
    }

    if ( filesDontExist > 0 ) {
        string message = "Couldn't find " + ofToString(filesDontExist) + " sound files. "
                                                                         "This may lead to unexpected errors.";
        Gui::getInstance()->showMessage(message, "Error");
    }

}

void Sounds::reset() {
    AudioEngine::getInstance()->engine.stop();
    
    mainQuadTree.reset();
    for (int i = 0; i < sounds.size(); i++) {
        delete sounds[i];
    }
    sounds.clear();
    clusterNames.clear();
    soundPositions.clear();
    idCount = 0;
    backgroundAlpha = BG_ALPHA_MIN;
    
    AudioEngine::getInstance()->engine.start();
}

void Sounds::update() {
    for (int i = 0; i < sounds.size(); i++) {
        sounds[i]->update();
    }

    animateClusterNames();
}

void Sounds::draw() {
    ofFill();
    drawBackground();    
    ofSetColor(255,255,255,255);

    if (clusterNamesOpacity > 0) {

        ofSetColor(ofColor::white, clusterNamesOpacity);

        for ( int i = 0 ; i < hulls.size() ; i++ ) {
            ImVec2 textSize = ImGui::CalcTextSize(clusterNames[i].c_str(), NULL, true);
            font.drawString(clusterNames[i], hulls[i].getCentroid2D().x - (textSize.x / 2), hulls[i].getCentroid2D().y);
        }
    }



    //ofSetCircleResolution(100);
    for (int i = 0; i < sounds.size(); i++) {
        if(!sounds[i]->getHide()) {
            sounds[i]->draw();

        }
        if ( showSoundFilenamesTooltip && sounds[i]->getHovered() ) {
            sounds[i]->drawGui();
        }
    }

//    mainQuadTree.draw();
}

void Sounds::drawGui(){
// this is just confusing and doesn't add any value
    //
//    if(ImGui::Checkbox("Sounds original positions", &useOriginalPositions)){
//        setUseOriginalPositions();
//    }
//    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::SOUNDS_ORIGINAL_POSITIONS);
}

void Sounds::drawBackground()
{
    ofPushMatrix();
    ofTranslate( -windowOriginalWidth*BG_TRANSLATION, -windowOriginalHeight*BG_TRANSLATION );
    ofSetColor(255,255,255,round(backgroundAlpha));
    fboBackground.draw(0,0);
    
    backgroundAlpha += BG_ALPHA_STEP * backgroundDirection * ofGetLastFrameTime() * 40;
    
    if ( backgroundAlpha >= BG_ALPHA_MAX ) {
        backgroundAlpha = BG_ALPHA_MAX;
        backgroundDirection = -1;
    } else if ( backgroundAlpha <= BG_ALPHA_MIN ) {
        backgroundAlpha = BG_ALPHA_MIN;
        backgroundDirection = 1;
    }
    ofPopMatrix();
}

void Sounds::generateBackground()
{
    fboBackground.allocate(windowOriginalWidth * 1.5, windowOriginalHeight * 1.5, GL_RGBA);
    
    fboBackground.begin();
    ofClear(25,0);
    ofPushMatrix();
    ofTranslate( windowOriginalWidth * BG_TRANSLATION, windowOriginalHeight * BG_TRANSLATION );
    
    float glowSize = SIZE_ORIGINAL * BG_GLOW_SIZE;
    
    for ( auto * sound : sounds ) {
        if ( sound->getCluster() < 0 ) {
            continue;
        }
        ofSetColor( ColorPaletteGenerator::getColor(sound->getCluster()) );
        //            ofDrawCircle(position.x, position.y, glowBigSize);
        Sound::imgGlow.draw(sound->position.x - glowSize / 2,
                            sound->position.y - glowSize / 2,
                            glowSize,
                            glowSize);
    }
    
    ofNoFill();
    ofSetColor(255,0,0);
    ofPopMatrix();
    //       ofDrawRectangle(1,1,fboBackground.getWidth()-2, fboBackground.getHeight()-1);
    fboBackground.end();
}

void Sounds::animateClusterNames() {
    if (showClusterNames){
        if (clusterNamesOpacity < 255){
            clusterNamesOpacity += clusterNamesSpeed;
        }
    } else {
        if (clusterNamesOpacity > 0){
            clusterNamesOpacity -= clusterNamesSpeed;
        }
    }
}

void Sounds::findConvexHulls() {
    ofxConvexHull convexHull;
    vector<ofVec3f> cluster;
    string strDebug = "clusterIDS: ";
    
    hulls.clear();
    
    for ( auto clusterId : clusterIds ) {
        strDebug += ofToString(clusterId) + ",";
        cluster.clear();
        for ( auto * sound : sounds ) {
            if ( sound->getCluster() == clusterId ) {
                cluster.push_back( sound->getPosition() );
            }
        }
        
        ofPolyline polyHull;
        if ( cluster.size() >= 3 ) {
            vector<ofPoint> hull = convexHull.getConvexHull( cluster );
            
            for ( int i = 0 ; i < hull.size() ; i++ ) {
                polyHull.addVertex( hull[i] );
            }
            polyHull.close();
            
            //        agrando desde el centro
            for( int i = 0 ; i < polyHull.size() ; i++ ) {
                auto &p = polyHull.getVertices()[i];
                
                p.x += polyHull.getNormalAtIndex(i).x * 15;
                p.y += polyHull.getNormalAtIndex(i).y * 15;
            }
        }
        
        hulls.push_back( polyHull );
    }
}

void Sounds::updateSoundPosition(Sound * sound) {
    soundPosition * found = nullptr;
    for ( int i = 0 ; i < soundPositions.size() ; i++ ) {
        if ( soundPositions[i].sound == sound ) {
            found = &soundPositions[i];
            break;
        }
    }

    if ( found == nullptr ) {
        found = new soundPosition;
        found->sound = sound;
        
        soundPositions.push_back(*found);
    }
    
    found->position.x = sound->position.x;
    found->position.y = sound->position.y;
}

void Sounds::mouseMoved(int x, int y, bool onGui) {
    if ( onGui ) {
        // this never happens and not sure if it is clever performance-wise
        for ( auto * sound : sounds ) {
            sound->clusterIsHovered = true;
        }
        return;
    }
    
    Sound * nearestSound = getNearestSound( ofVec2f(x,y) );
    allSoundsHoveredOff(nearestSound);
    if (nearestSound != NULL) {
        if ( !nearestSound->getHovered() ) {
            nearestSound->setHovered(true);
        }
    }

    //Select cluster
    set<int> selectedClusters;
    selectedCluster = -1;
    for ( int i = 0 ; i < hulls.size() ; i++ ) {
        if ( hulls[i].size() >= 3 && hulls[i].inside( x, y) ) {
            selectedClusters.insert(i);
        }
    }
    
    for ( auto &cluster : selectedClusters ) {
        if ( selectedCluster == -1 ) {
            selectedCluster = cluster;
        } else {
            if ( hulls[cluster].getArea() < hulls[selectedCluster].getArea() ) {
                selectedCluster = cluster;
            }
        }
    }
    
    hoveredClusterID = selectedCluster;
    
    for ( auto * sound : sounds ) {
        if ( (selectedCluster == -1 && !showClusterNames)|| sound->getCluster() == selectedCluster ) {
            sound->clusterIsHovered = true;
        } else {
            sound->clusterIsHovered = false;
        }
    }
}

void Sounds::mouseDragged(ofVec2f p, int button) {
    if ( !useOriginalPositions && button == 2 ) {
        Sound * hoveredSound = getHoveredSound();
        
        if ( hoveredSound != nullptr ) {
            hoveredSound->position = p;
            updateSoundPosition(hoveredSound);
            soundIsBeingMoved = true;
        }
    }
}

void Sounds::mousePressed(int x, int y, int button) {

}

void Sounds::mouseReleased(int x, int y, int button)
{
    //end of drag
    if ( soundIsBeingMoved ) {
        sequenceUnitReprocess();
        mainQuadTree.reprocess( sounds );
        soundIsBeingMoved = false;
    }
}


void Sounds::allSoundsSelectedOff() {
    for(int i = 0; i < sounds.size(); i++) {
        sounds[i]->selected = false;
    }
}
void Sounds::allSoundsHoveredOff(Sound * except) {
    for(int i = 0; i < sounds.size(); i++) {
        if ( except != NULL && sounds[i] == except ) {
            continue;
        }
        if ( sounds[i]->getHovered() ) {
            sounds[i]->setHovered(false);
        }
    }
}

Sound * Sounds::getNearestSound(ofVec2f position) {

    if ( sounds.size() == 0 ) {
        return nullptr;
    }
    vector<Sound*> *ss= mainQuadTree.search(position);
    //ofLogNotice("S", ofToString(ss->size()));
    if(ss == nullptr || ss->size() == 0 ){
        return nullptr;
    }

   float minDistance = (*ss).at(0)->getPosition().squareDistance(position);
   int soundIdx = -1;
   for(unsigned int i = 0; i < ss->size(); i ++){
       Sound * s = (*ss).at(i);
       if(s->getHide()){
           continue;
       }
       float dist = s->getPosition().squareDistance(position);

       if ( dist > pow(s->size * 3,2) ) {
                continue;
       }
       if(dist <= minDistance){
           minDistance = dist;
           soundIdx = i;
       }
   }
   if(soundIdx != -1){
      return (*ss).at(soundIdx);
   }else{
       return nullptr;
   }
}

Sound * Sounds::getHoveredSound() {
    for(int i = 0; i < sounds.size(); i++) {
        if ( sounds[i]->hovered ) {
            return sounds[i];
        }
    }
    return nullptr;
}

void Sounds::setSpaceLimits(vector<string> soundsCoords) {
    ofVec3f max(-99999999, -99999999, -99999999);
    ofVec3f min(99999999, 99999999, 99999999);
    // spaceLimits = {ofVec2f(0,0),ofVec2f(1500,1500)};
    spaceLimits[0] = max;
    spaceLimits[1] = min;
    
    
    for(int i = 0; i< soundsCoords.size() - 1; i++) {
        if(soundsCoords[i] == "")continue;
        auto lineElements = ofSplitString(soundsCoords[i],",");
        
        ofVec3f position(ofToFloat(lineElements[TSV_FIELD_X]), ofToFloat(lineElements[TSV_FIELD_Y]), ofToFloat(lineElements[TSV_FIELD_Z]));
        
        spaceLimits[0].x = Utils::getMaxVal(position.x, spaceLimits[0].x);
        spaceLimits[0].y = Utils::getMaxVal(position.y, spaceLimits[0].y);
        spaceLimits[0].z = Utils::getMaxVal(position.z, spaceLimits[0].z);
        
        spaceLimits[1].x = Utils::getMinVal(position.x, spaceLimits[1].x);
        spaceLimits[1].y = Utils::getMinVal(position.y, spaceLimits[1].y);
        spaceLimits[1].z = Utils::getMinVal(position.z, spaceLimits[1].z);
        
    }
    
}

void Sounds::setClusteringOptionsScreenFlags(){
    haveToDrawClusteringOptionsScreen = true;
}

void Sounds::drawClusterNames()
{
    if (clusterNamesOpacity > 0) {
        ofSetColor(ofColor::white, clusterNamesOpacity);

        for ( int i = 0 ; i < hulls.size() ; i++ ) {
            ImVec2 textSize = ImGui::CalcTextSize(clusterNames[i].c_str(), NULL, true);
            font.drawString(clusterNames[i], hulls[i].getCentroid2D().x - (textSize.x / 2), hulls[i].getCentroid2D().y);
        }
    }
}

void Sounds::drawClusteringOptionsScreen(){
    if(haveToDrawClusteringOptionsScreen){
        ImGuiWindowFlags window_flags = 0;
        window_flags |= ImGuiWindowFlags_NoScrollbar;
        window_flags |= ImGuiWindowFlags_NoResize;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoNav;
        window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

        if(ImGui::Begin("Clustering settings",
                        &haveToDrawClusteringOptionsScreen,
                        window_flags)){

            ImGui::Text("DBScan Settings");

            if( useOriginalClusters || areAnyClusterNames()){
                if ( useOriginalClusters ) {
                    if(ImGui::Button("Forget original clusters and enable cluster options")){
                        useOriginalClusters = false;
                        deleteClusterNames();
                    }
                } else {
                    if(ImGui::Button("Delete cluster names and enable cluster options")){
                        deleteClusterNames();
                    }
                }
                ImGui::NewLine();
            }else{
//                    ImGui::SliderInt("Eps", &epsDbScan, 5, 100);
                if (ImGui::InputInt("Eps", &epsDbScan)) {
                    epsDbScan = ofClamp(epsDbScan, 5, 500);
                    doClustering();
                }
                if(ImGui::IsItemHovered())  Tooltip::setTooltip(Tooltip::CLUSTERS_EPS);

//                    ImGui::SliderInt("MinPts", &minPts, 3, 100);
                if (ImGui::InputInt("MinPts", &minPts)) {
                    minPts = ofClamp(minPts, 3, 100);
                    doClustering();
                }
                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::CLUSTERS_MIN_PTS);
            }
        }
        ImGui::End();
    }
}

void Sounds::setUseOriginalPositions() {
    for(unsigned int i = 0; i < soundPositions.size(); i++) {
        if (useOriginalPositions) {
            soundPositions[i].sound->useOriginalPosition();
        } else {
            soundPositions[i].sound->position = soundPositions[i].position;
        }
    }
    mainQuadTree.reprocess(sounds);
}

void Sounds::revertToOriginalPositions()
{
    for(unsigned int i = 0; i < soundPositions.size(); i++) {
        soundPositions[i].sound->useOriginalPosition();
    }
    soundPositions.clear();

    sequenceUnitReprocess();
    mainQuadTree.reprocess(sounds);
}

void Sounds::sequenceUnitReprocess()
{
    for ( auto u : Units::getInstance()->currentUnits ) {
        if ( u->getUnitName() == "Sequence Unit" ) {
            auto seqUnit = dynamic_pointer_cast<SequenceUnit>(u);
            seqUnit->processSequence();
        }
    }
}

void Sounds::onClusterToggle(int clusterIdx) {
    for(unsigned int i = 0; i<sounds.size(); i++) {
        if(sounds[i]->getCluster() == clusterIdx) {
            sounds[i]->toggleHide();
        }
        
    }
}

void Sounds::doClustering()
{
    DBScan dbscan( epsDbScan, minPts, getSoundsAsPoints());
    dbscan.run();
    setDBScanClusters(dbscan.points);
    findConvexHulls();
    generateBackground();
}

Sound * Sounds::getSoundByFilename(string filename) {
    for ( int i = 0 ; i < sounds.size() ; i++ ) {
        if ( sounds[i]->getFileName() == filename ) {
            return sounds[i];
        }
    }
    return NULL;
}
Sound * Sounds::getSoundById(int id) {
    for ( int i = 0 ; i < sounds.size() ; i++ ) {
        if ( sounds[i]->id == id ) {
            return sounds[i];
        }
    }
    return NULL;
}

vector<Sound *> Sounds::getSounds()
{
    return sounds;
}

vector<Sound *> Sounds::getSoundsByCluster(int clusterId)
{
    vector<Sound *> ret;
    for ( int i = 0 ; i < sounds.size() ; i++ ) {
        if ( sounds[i]->getCluster() == clusterId ) {
            ret.push_back( sounds[i] );
        }
    }
    return ret;
}

vector<Sound *> Sounds::getSoundsByCluster(string name){
    vector<Sound *> snds;
    int clusterId = -1;

    for ( int i = 0 ; i < clusterNames.size(); i++){
        if (clusterNames[i] == name){
            clusterId = i;
            break;
        }
    }

    if ( clusterId != -1 ) {
        for ( int i = 0 ; i < sounds.size() ; i++ ) {
            if ( sounds[i]->getCluster() == clusterId ) {
                snds.push_back( sounds[i] );
            }
        }
    }

    return snds;

}

vector<Sound *> Sounds::getNeighbors(Sound *s, float threshold)
{
    ofVec2f position = s->getPosition();
    return getNeighbors(position, threshold);
}

vector<Sound *> Sounds::getNeighbors(ofVec2f position, float threshold)
{
    vector<Sound *> neighbors;
    
    for ( int i = 0 ; i < sounds.size() ; i++ ) {
        ofVec2f p = sounds[i]->getPosition();
        if ( p != position && p.squareDistance( position ) <= threshold ) {
            neighbors.push_back(sounds[i]);
        }
    }
    
    return neighbors;
}

vector<ofPoint> Sounds::getSoundsAsPoints(bool originalPositions)
{
    vector<ofPoint> soundsAsPoints;
    
    for ( int i = 0 ; i < sounds.size() ; i++ ) {
        if ( originalPositions ) {
            soundsAsPoints.push_back( sounds[i]->getOriginalPosition() );
        } else {
            soundsAsPoints.push_back( sounds[i]->getPosition() );
        }
    }
    
    return soundsAsPoints;
}

void Sounds::setDBScanClusters(vector<dbscanPoint> points)
{
    clusterIds.clear();
    
    for ( int i = 0 ; i < sounds.size() ; i++ ) {
        sounds[i]->setCluster( points[i].cluster );
        
        if ( points[i].cluster >= 0 ) {
            clusterIds.insert( points[i].cluster );
            //Para saber si cada cluster se está mostrando. Asumo que todos los clusters empiezan siendo mostrados...
            clustersShow.push_back(true);
        }
    }
    nClusters = clusterIds.size();
    for( int i = 0; i < nClusters; i++){
        clusterNames.push_back("");
    }
}

void Sounds::setFilenameLabel(string fileName){
    currentSoundLabel = fileName;
}

bool Sounds::getUseOriginalPositions(){
    return useOriginalPositions;
}

bool Sounds::getUseOriginalClusters()
{
    return useOriginalClusters;
}

bool Sounds::getShowSoundFilenames(){
    return showSoundFilenamesTooltip;
}

int Sounds::getInitialWindowWidth()
{
    return windowOriginalWidth;
}

int Sounds::getInitialWindowHeight()
{
    return windowOriginalHeight;
}

ofVec3f Sounds::camToSoundCoordinates(ofVec2f camCoordinates) {
    return camToSoundCoordinates( ofVec3f(camCoordinates.x, camCoordinates.y, 0) );
}

ofVec3f Sounds::camToSoundCoordinates(ofVec3f camCoordinates)
{
    ofVec3f r;
    
    r.x = ofMap(camCoordinates.x,
                0 + spacePadding,
                windowOriginalWidth - spacePadding,
                spaceLimits[1].x,
                spaceLimits[0].x);
    
    
    r.y = ofMap(camCoordinates.y,
                0 + spacePadding,
                windowOriginalHeight - spacePadding,
                spaceLimits[1].y,
                spaceLimits[0].y);
    
    r.z = ofMap(camCoordinates.z,
                0.5,1,
                spaceLimits[1].z,
                spaceLimits[0].z);
    
    return r;
}

ofVec3f Sounds::soundToCamCoordinates(ofVec3f soundCoordinates)
{
    ofVec3f r;
    
    r.x = ofMap(soundCoordinates.x,
                spaceLimits[1].x,
                spaceLimits[0].x,
                0 + spacePadding,
                windowOriginalWidth - spacePadding);
    
    r.y = ofMap(soundCoordinates.y,
                spaceLimits[1].y,
                spaceLimits[0].y,
                0 + spacePadding,
                windowOriginalHeight - spacePadding);
    
    r.z = ofMap(soundCoordinates.z,
                spaceLimits[1].z,
                spaceLimits[0].z,
                0.5,1);
    
    return r;
}

void Sounds::setUseOrigPos(bool v){
    useOriginalPositions = v;
}

void Sounds::setShowSoundFilenames(bool v){
    showSoundFilenamesTooltip = v;
}

unsigned int Sounds::getSoundCount()
{
    return sounds.size();
}

Json::Value Sounds::save() {
    Json::Value root = Json::Value( Json::objectValue );
    Json::Value overridedPositions = Json::Value( Json::arrayValue );
    
    for ( auto sPos : soundPositions ) {
        Json::Value soundPosition = Json::Value( Json::objectValue );
        soundPosition["id"] = sPos.sound->id;
        soundPosition["position"] = Json::Value( Json::arrayValue );
        soundPosition["position"].append( sPos.position.x );
        soundPosition["position"].append( sPos.position.y );
        
        overridedPositions.append(soundPosition);
    }

    //cluster names
    Json::Value jsonClusterNames = Json::Value(Json::arrayValue);
    for (auto clusterName : clusterNames ) {
        Json::Value jsonClusterName = Json::Value(Json::stringValue);
        jsonClusterName = clusterName;
        jsonClusterNames.append(jsonClusterName);
    }

    root["clusterNames"] = jsonClusterNames;
    root["useOriginalClusters"] = useOriginalClusters;
    root["overridedPositions"] = overridedPositions;
    root["DBScan_EPS"] = epsDbScan;
    root["DBScan_minPts"] = minPts;


    //muted sounds
    Json::Value muted = Json::Value(Json::arrayValue);
    for (auto sound : sounds) {
        if (sound->mute) {
            muted.append(sound->id);
        }
    }

    root["muted"] = muted;

    return root;
}

void Sounds::load( Json::Value jsonData ) {
    Json::Value overridedPositions = jsonData["overridedPositions"];
    for ( int i = 0 ; i < overridedPositions.size() ; i++ ) {
        // Sound * sound = getSoundById( overridedPositions[i]["id"] )
        soundPosition newSoundPosition;
        newSoundPosition.sound = getSoundById( overridedPositions[i]["id"].asInt() );
        
        if ( newSoundPosition.sound == NULL ) {
            ofLog() << "Sound ID not found when loading session";
            continue;
        }
        
        newSoundPosition.position.x = overridedPositions[i]["position"][0].asInt();
        newSoundPosition.position.y = overridedPositions[i]["position"][1].asInt();
        
        soundPositions.push_back(newSoundPosition);
        setUseOriginalPositions();
    }
    
    Json::Value names = jsonData["clusterNames"];
    if(names != Json::nullValue){
        for(unsigned int i = 0; i < names.size(); i++){
           clusterNames.push_back(names[i].asString());
        }
    }
    
    if ( jsonData["DBScan_EPS"].isInt() ) {
        epsDbScan = jsonData["DBScan_EPS"].asInt();
    }
    if ( jsonData["DBScan_minPts"].isInt() ) {
        minPts = jsonData["DBScan_minPts"].asInt();
    }
    
    useOriginalClusters = jsonData["useOriginalClusters"].isBool() &&
                          jsonData["useOriginalClusters"].asBool();

    if ( !useOriginalClusters ) {
        doClustering();
    } else {
        generateBackground();
    }

    Json::Value muted = jsonData["muted"];
    if (muted != Json::nullValue) {
        for (int i = 0; i < muted.size(); i++) {
            sounds[muted[i].asInt()]->mute = true;
        }
    }

}

string Sounds::selectFolder() {
    
    ofFileDialogResult dialogResult = ofSystemLoadDialog("Select Folder", true, ofFilePath::getUserHomeDir());
    
    string path;
    
    if(dialogResult.bSuccess) {
        path = dialogResult.getPath();
    }
    
    return path;
}

void Sounds::exportFiles() {
    
    string selectedFolderPath = selectFolder();
    
    if (selectedFolderPath.size() > 0) {
        
        selectedFolderPath = ofFilePath::addTrailingSlash(selectedFolderPath);
        
        for (std::set<int>::iterator it=clusterIds.begin(); it!=clusterIds.end(); ++it) {
            
            string clusterId = to_string(*it);
            string clusterFolderPath = selectedFolderPath + clusterId;
            
            ofDirectory dir(clusterFolderPath);
            if(!dir.exists()){
                dir.create(true);
            }
        }
        
        for(int i = 0; i < sounds.size(); i++) {
            
            if (sounds[i]->getCluster() >= 0 ) {
                
                string cluster = to_string( sounds[i]->getCluster() );
                string sourcePath = sounds[i]->getFileName();
                string destinationPath = selectedFolderPath + cluster;
                
                ofFile::copyFromTo(sourcePath, destinationPath, true, false);

            }
        }
        
        for (int i = 0; i < clusterNames.size(); i++) {
            
            if (clusterNames[i] != "") {
                
                string clusterFolderPath = selectedFolderPath + ofToString(i);
                ofDirectory dir(clusterFolderPath);
                
                if(dir.exists()){
                    string newFolderName = selectedFolderPath + ofToString(i) + " - " + clusterNames[i];
                    dir.renameTo(newFolderName);

                }
            }
        }

        Gui::getInstance()->showMessage("All sounds have been saved to folders", "Information", true);
    }
}

void Sounds::keyPressed(ofKeyEventArgs & e){
    if(e.key == CLUSTERNAME_SHOW_SHORTCUT && !showClusterNames) {
        showClusterNames = true;
        for (unsigned int i = 0; i < sounds.size(); i++) {
            sounds[i]->clusterIsHovered = false;
        }
    }
}

void Sounds::keyReleased(int key){
    if(key == CLUSTERNAME_SHOW_SHORTCUT) {
        showClusterNames = false;
        for (unsigned int i = 0; i < sounds.size(); i++) {
            sounds[i]->clusterIsHovered = true;
        }
    }
}

void Sounds::nameCluster(unsigned int clusterID){
        clusterNames[clusterID] = ofToString(clusterNameBeingEdited);
        clusterNameBeingEdited[0] = '\0';
}

bool Sounds::areAnyClusterNames(){
    for(unsigned int i = 0; i < clusterNames.size(); i++){
        if(clusterNames[i] != "") return true;

    }
    return false;
}

void Sounds::deleteClusterNames(){
    for(unsigned int i = 0; i < clusterNames.size(); i++){
       clusterNames[i]  = "";
    }
}
