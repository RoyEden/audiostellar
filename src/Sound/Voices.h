#pragma once

#include "ofMain.h"
#include "ofxPDSP.h"
#include "AudioEngine.h"
#include "Sounds.h"
#include "ofxJSON.h"

class Voice : public pdsp::Patchable {

public:

    Voice() {};
    Voice(const Voice & other) {};

    AudioEngine * audioEngine = AudioEngine::getInstance();

    Sound * sound = nullptr;

    void setup();

    void load(string path);
    void unload();
    void play(float volume = 1.0f);
    void stop();
    bool isLoaded();
    bool isPlaying();
    float getPlayerPosition();
    void setVolume(float volume);
    void setOutputs(int channel0, int channel1);
    void setOutputs();

private:

    pdsp::Sampler sampler0;
    pdsp::Sampler sampler1;
    pdsp::ADSR env;
    pdsp::Amp amp0;
    pdsp::Amp amp1;
    pdsp::Amp volume0;
    pdsp::Amp volume1;
    pdsp::TriggerControl sampleTrigger;
    pdsp::TriggerControl envTrigger;

    pdsp::SampleBuffer sample;
    bool isPatched = false;
};

class Voices : public pdsp::Patchable {
private:
    static Voices* instance;
    Voices();

public:
    static Voices* getInstance();

    AudioEngine * audioEngine;

    int numVoices = 500;

    vector <Voice> voices;

    void setup();
    void reset();

    bool isPlaying(Sound * sound);
    
    int loadedVoices = 0;

    Voice * getVoice(Sound * sound);
    Voice * getNonPlayingVoice(Sound * sound);
    Voice * getFreeVoice();
    Voice * getReplaceableVoice();
    
    void takeOverVoice(Sound * sound, Voice * voice);
    
    map < Sound *, vector <Voice *> > voiceMap;
};

